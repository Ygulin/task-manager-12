package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
